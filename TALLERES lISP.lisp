;;TALLERES LISP

;TALLER 1
;1- EVALUE LAS SIGUIENTES EXPRESIONES:
( * ( / ( + 3 3 ) ( - 4 4 ) ) ( * 0 9) )  ;A
(+ (* 3 (- 5 3)) (/ 8 4))                 ;B
(* 3 2 2 (- 8 5))                         ;C
( ( * ) ( * 2 ( - 4 2 ) ) ( / 8 2))       ;D

;2- EVALUE LAS SIGUIENTES FORMAS:
;SQRT=RAIZ CUADRADA ABS= VALOR ABSOLUTO EXPT=ELEVAR POTENCIA LOG=LOGARTIMO NATURAL MOD=RESTO DE LA DIVISION
( SQRT ( ABS ( - 71 ( EXPT ( + 2 4 )  4 ) ) ) ) ;A
'( '+ '1 '( * 2 4) )                            ;B
( EXPT ( MOD 5 3 ) ( ABS ( - 8 9 ) ) )          ;C
( QUOTE ( NIL 'NIL T 'T) )                      ;D
(LOG (EXPT 2 (- 15 5 4)))                       ;E
(QUOTE (QUOTE (HELLO BYEBYE)))                  ;F

;TALLER 2
;SETQ: REALIZA ASIGNACIONES DE VARIABLES POR PAREJAS A=A, B=6, C=5
( SETQ A 4 B 6 C 5 X ( + A B ) Y ( - B C ) Z ( MAX A C ) ) ;A
(+ A B C)                                                  ;B
(EVAL X)                                                   ;C
(EVAL Y)                                                   ;D
(EVAL Z)                                                   ;E

;TALLER 3
;SEA ( SETQ A 4 B 6 C 5 X ( + A B ) Y ( - B C ) Z ( MAX A C ) )
;EVALUE LAS EXPRESIONES A CONTINUACION:
( + ( * C Z ) B C)                    ;A
( ABS ( + ( * ( - Z X A ) - 100 ) B) );B
( * ( + ( * C X ) ( - A Z C ) ) 2 Y)  ;C

;AHORA SE TIENE QUE:
;( SETQ M ( + Z A) N ( - Y C ) P ( * 2 Z ) )
( + M Z X Y P B N )                       ;D
( - ( - ( * 3 Z ) ( / 100 C ) ) A C N P ) ;E

 ;TALLER 4
 ;4- EVALUE LAS SIGUIENTES EXPRESIONES
 (CONS(CAR '( AXL WIL RICH)) (CDR '(ESTE ANTO ALLA)));A
 (CONS (CDAR '((CONS GO UP))) (THIRD '(WE FIND (ALL FINE))));B
 (CAR (CDR (CAR (CDR '((A B) (C D) (E F))))));C
 (CAR (CAR (CDR (CDR '((A B) (C D) (E F))))));D
 (CAR (CAR (CDR '(CDR ((A B) (C D) (E F))))));E
 '(CAR (CAR (CDR (CDR ((A B) (C D) (E F))))));F
 (CONS (CAR NIL) (CDR NIL))                  ;G

 ;5- EVALUE LAS SIGUIENTES FORMAS:
 (CDR (CAR ) (CDR ( CAR ')))
